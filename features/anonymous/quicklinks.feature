#features/anonymous/quicklinks.feature

@makefile
Feature: Quick Links

  Background:
    Given I am on the homepage

  Scenario: check that the Calendar quick link goes to /academic_calendar
    When I follow "Calendar"
    Then the url should match "/academic_calendar"

  Scenario: check that the Directory quick link goes to /directory
    When I follow "Directory"
    Then the url should match "/directory"

  Scenario: check that the Index quick link goes to /index
    When I follow "Index"
    Then the url should match "/index"

  Scenario: check that the Map quick link goes to /campusmaps
    When I follow "Map"
    Then the url should match "/campusmaps"

  Scenario: check that the myWestern quick link goes to /mywestern
    When I follow "myWestern"
    Then the url should match "/mywestern/*"

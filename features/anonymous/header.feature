#features/anonymous/header.feature

@javascript
Feature: Header

  Background:
    Given I am on the homepage

  Scenario: check that the banner and logo are visible
    Then I should see an element with the selector ".site-banner img"
  	And I should see an element with the selector ".western-logo"

#features/anonymous/socialmediaicons.feature

Feature: Footer Social Media Icons

  Background:
    Given I am on the homepage

  Scenario: Facebook
    When I click on the element with the selector "a.westernIcons-FacebookIcon"
    Then the response status code should be 200

  Scenario: Flickr
    When I click on the element with the selector "a.westernIcons-FlickrIcon"
    Then the response status code should be 200

  Scenario: Twitter
    When I click on the element with the selector "a.westernIcons-TwitterIcon"
    Then the response status code should be 200

  Scenario: YouTube
    When I click on the element with the selector "a.westernIcons-YouTubeIcon"
    Then the response status code should be 200

  Scenario: Google+
    When I click on the element with the selector "a.westernIcons-GooglePlusIcon"
    Then the response status code should be 200

  Scenario: RSS
    When I click on the element with the selector "a.westernIcons-RSSicon"
    Then the response status code should be 200


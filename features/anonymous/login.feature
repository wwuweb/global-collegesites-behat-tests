#features/anonymous/login.feature

Feature: Login

  Scenario: check for presence of the login fields on the /user page
    Given I am at "/user"
    Then I should see the heading "User account"
    And I should see "Log in"

  Scenario: request a new password - go to page and look for vitals
    Given I am at "/user"
    When I follow "Request new password"
    Then I should be on "/user/password"
    And I should see the heading "User account"

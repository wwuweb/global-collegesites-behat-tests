#features/anonymous/mobilebuttons.feature

@javascript
Feature: Mobile Buttons

  Background:
    Given I am on the homepage
    And the browser has the dimensions "800" by "600"

  Scenario: check for the western search button and that it remains open across browser resize
    Then I should see an element with the selector ".western-search > button"
    When I press the "Open Search" button
    Then I should see an element with the selector ".western-search-widget"
    When I resize the browser to the dimensions "801" by "600"
    Then I should see an element with the selector ".western-search-widget"
    When I resize the browser to the dimensions "800" by "600"
    Then I should see an element with the selector ".western-search-widget"

  Scenario: check that the quick links toggle correctly
  	Then I should not see an element with the selector ".western-quick-links ul"
    When I press the "Toggle Quick Links" button
    Then I should see an element with the selector ".western-quick-links ul"
    And I should see the link "Calendar" 
    And I should see the link "Directory"
    And I should see the link "Index" 
    And I should see the link "Map" 
    And I should see the link "myWestern"
    When I press the "Toggle Quick Links" button
    And I wait 1 seconds
    Then I should not see an element with the selector ".western-quick-links ul"
    When I resize the browser to the dimensions "801" by "600"
    Then I should see an element with the selector ".western-quick-links ul"

  Scenario: check for the mobile main menu button
    Then I should see an element with the selector ".mobile-main-nav"
    When I resize the browser to the dimensions "801" by "600"
    Then I should not see an element with the selector ".mobile-main-nav"

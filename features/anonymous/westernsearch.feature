#features/anonymous/westernsearch.feature

@javascript
Feature: Western Search

  Background:
    Given I am on the homepage

  Scenario: check for the western search button
    Then I should see an element with the selector ".western-search > button"

  Scenario: check that the Western Search Widget appears and disappears
    Then I should not see an element with the selector ".western-search-widget"
    When I press the "Open Search" button
    Then I should see an element with the selector ".western-search-widget"
    When I press the "Open Search" button
    Then I should not see an element with the selector ".western-search-widget"

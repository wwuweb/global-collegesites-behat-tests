#features/anonymous/footer.feature

Feature: Footer

  Background:
    Given I am on the homepage

  Scenario: check for contact information in the footer
    Then I should see "516 High Street"
    And I should see "(360) 650-3000"
    And I should see the link "Contact Western"

  Scenario: test contact email link
    When I follow "Contact Western"
    Then I should see the heading "Contact Us"

  Scenario: Check for privacy link
    When I click "Website Privacy Statement"
    Then I should see the heading "Online Privacy Statement"

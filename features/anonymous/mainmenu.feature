#features/anonymous/mainmenu.feature

@javascript @prod-only
Feature: Main Menu

  Scenario: check for the main menu selector
    Given I am on the homepage
    Then I should see an element with the selector "#main-menu div > ul.menu"
    
  Scenario: Check toggling in mobile view
  	Given the browser has the dimensions "800" by "600"
  	Then I should not see an element with the selector "#main-menu div > ul.menu"
  	When I click the element with the selector ".mobile-main-nav"
  	Then I should see an element with the selector "#main-menu div > ul.menu"
  	When I click the element with the selector ".mobile-main-nav"
  	Then I should not see an element with the selector "#main-menu div > ul.menu"
  	When I resize the browser to the dimensions "801" by "600"
  	Then I should see an element with the selector "#main-menu div > ul.menu"

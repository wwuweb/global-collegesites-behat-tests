#features/anonymous/jsclass.feature

@javascript
Feature: "js" class

  Scenario: Check that the "js" class is present on the html tag
    Given I am on the homepage
    Then I should see an element with the selector "html.js"

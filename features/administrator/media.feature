#features/media.feature

@api @javascript @media
Feature: Test functionality of the Media and ancilliary modules

  Background:
    Given the "media_wysiwyg" module is enabled
    And the "admin_menu" module is disabled
    And the "toolbar" module is enabled
    And I am logged in as a user with the "administrator" role

  Scenario Outline: Media web insert
    Given I am at "/node/add/page"
    And for "edit-title" I enter <title>
    When I click on the element with the selector ".cke_button__media"
    And I wait for AJAX to finish
    And I focus on the iframe "mediaBrowser"
    And I click on the element with the selector "[title=Web]"
    And for "edit-embed-code" I enter <code>
    And I click on the element with the selector "#media-internet-add-upload #edit-next"
    And I wait for AJAX to finish
    And I press "Save"
    And I refocus on the iframe "mediaStyleSelector"
    And I click on the fake "Submit" button
    And I reset to the base iframe
    And I press "Save"
    And I reset to the base iframe
    Then I should see the message <message>
    And I should see an element with the selector "[data-media-element=1]"

    Examples:
      | title               | code                          | message                                          |
      | "YouTube test page" | "http://youtu.be/CBOV92CVgV8" | "Basic page YouTube test page has been created." |
      | "Vimeo test page"   | "http://vimeo.com/20302524"   | "Basic page Vimeo test page has been created."   |

  @file
  Scenario: Check that it is possible to upload a file through the WYSIWYG editor
    Given I am at "/node/add/page"
    And for "edit-title" I enter "File upload test page"
    When I click on the element with the selector ".cke_button__media"
    And I wait for AJAX to finish
    And I focus on the iframe "mediaBrowser"
    And I click on the element with the selector "[title=Upload]"
    And I attach the file "test_image.png" to "edit-upload-upload"
    And I wait for AJAX to finish
    And I press "Next"
    And I wait for AJAX to finish
    And I press "Save"
    And I refocus on the iframe "mediaStyleSelector"
    And I click on the fake "Submit" button
    And I reset to the base iframe
    And I press "Save"
    And I reset to the base iframe
    Then I should see the message "Basic page File upload test page has been created."
    And I should see an element with the selector ".media-element"

  @file
  Scenario: Check that it is possible to replace a file without the name or path changing
    Given I am at "/admin/content/file/list"
    When I click "test_image.png"
    And I click "Edit"
    And I wait for AJAX to finish
    And I focus on the iframe at index "1"
    And I attach the file "test_image.png" to "edit-replace-upload"
    And I press "Save"
    And I reset to the base iframe
    Then I should see the success message "Image test_image.png has been updated."
    But I should not see the success message containing "Image test_image.png has been deleted"

  @file
  Scenario: Check that it is possible to delete the file inserted in the previous test
    Given I am at "/admin/content/file/list"
    When I click "test_image.png"
    And I click "Delete"
    And I wait for AJAX to finish
    And I focus on the iframe at index "1"
    And I press "Delete"
    And I wait for AJAX to finish
    And I reset to the base iframe
    Then I should see the message "Image test_image.png has been deleted."

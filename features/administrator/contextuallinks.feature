#features/administrator/contextuallinks.feature

@javascript @api
Feature: Contextual Links

  Background:
    Given I am logged in as a user with the "administrator" role

  Scenario: Verify that the administrator user can access contextual links while logged in
    Given I am at "/admin/structure/block/manage/user/online/configure"
    When I select "content" from "edit-regions-wwuzen"
    And I press "Save block"
    And I go to the homepage
    Then I should see an element with the selector ".contextual-links-wrapper"

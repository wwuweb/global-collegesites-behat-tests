#features/administrator/authenticate.feature

@api
Feature: Authenticate user
  Demonstrate that a user can successfully log in to the site as an
  administrator by logging them in to the site, navigating to 'usrs/admin' and
  checking that the name admin appears in the page heading. 

  Scenario: Attempt to log in to the test site as an administrator
    Given I am logged in as a user with the "administrator" role
    And I am at "/users/admin"
    Then I should see the heading "admin"

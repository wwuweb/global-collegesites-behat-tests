<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;

use Drupal\DrupalExtension\Context\RawDrupalContext;

class AdmissionsContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * @Then I should see an event date
   */
  public function assertDatePresent() {
    $session = $this->getSession();
    $test_link = $session->getPage()->find('css', '.calendar a');

    if ($test_link === null) {
      throw new PendingException('No test dates appear on current calendar');
    }

    if ($test_link->getText() == 'Campus Tour' ||
      $test_link->getText() == 'Discovery Day' ||
      $test_link->getText() == 'Transfer Day') {
        $previous_url = $session->getCurrentUrl();
        $test_link->click();

        if ($previous_url === $session->getCurrentUrl()) {
          throw new PendingException('Did not change pages.');
        }

        $reg_time_link = $session->getPage()->find('css', '#WWU_VISIT_REG_TIME');

        if ($reg_time_link === null) {
          throw new Exception('Followed page but no reg code');
        }
      }
  }

}
